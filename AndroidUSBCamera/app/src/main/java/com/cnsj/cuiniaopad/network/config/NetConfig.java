package com.cnsj.cuiniaopad.network.config;

/**
 * Create time : 2019-1-11 0011
 * Author : steadyxia
 * Description : 网络配置类
 */
public class NetConfig {
    public static int HTTP_CONNECT_TIMEOUT = 30;//连接超时时间
    public static int HTTP_READ_TIMEOUT = 30;//读取超时时间
    public static int HTTP_WRITE_TIMEOUT = 30;//写入超时时间

    public static String CONNECT_SUCCESS = "200";
    public static String CONNECT_FAILURE = "500";
}
