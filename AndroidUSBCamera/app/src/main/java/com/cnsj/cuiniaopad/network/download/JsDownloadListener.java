package com.cnsj.cuiniaopad.network.download;

/**
 * create time :2019/4/13
 * author :steadyxia
 * description:
 */
public interface JsDownloadListener {
    void onStartDownload(long length);
    void onProgress(int progress);
    void onFail(String errorInfo);
}
