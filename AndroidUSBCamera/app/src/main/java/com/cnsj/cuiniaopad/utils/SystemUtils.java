package com.cnsj.cuiniaopad.utils;

import android.content.Context;
import android.os.PowerManager;
import android.os.SystemClock;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;


/**
 * 系统工具类
 * android:name="android.permission.DEVICE_POWER"
 */
public class SystemUtils {
    /**
     * 关闭屏幕 ，其实是使系统休眠
     */
    public static void goToSleep(Context context) {
        PowerManager powerManager = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        try {
            powerManager.getClass().getMethod("goToSleep", new Class[]{long.class}).invoke(powerManager, SystemClock.uptimeMillis());
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
    }

    /**
     * reboot 重启
     * input keyevent 26 电源键
     * input keyevent 223 息屏
     * input keyevent 224 亮屏
     * input keyevent 82 菜单锁
     * input keyevent 24 音量+键
     * input keyevent 25 音量-键
     * input keyevent 3 Home键
     * input keyevent 4 返回键
     * input keyevent 5 呼叫键
     * input keyevent 6 挂断键
     */

    /**
     * 息屏
     */
    public static void power() {
        String sleep = "input keyevent 26";
        execRootCmd(sleep);
    }

    /**
     * 息屏
     */
    public static void sleep() {
        String sleep = "input keyevent 223";
        execRootCmd(sleep);
    }

    /**
     * 唤醒
     */
    public static void wakeup() {
        String wakeup = "input keyevent 224";
        execRootCmd(wakeup);
    }


    /**
     * 执行命令并且输出结果
     */
    public static String execRootCmd(String cmd) {
        String result = "";
        DataOutputStream dos = null;
        DataInputStream dis = null;
        try {
            Process p = Runtime.getRuntime().exec("su");// 经过Root处理的android系统即有su命令
            dos = new DataOutputStream(p.getOutputStream());
            dis = new DataInputStream(p.getInputStream());
            dos.writeBytes(cmd + "\n");
            dos.flush();
            dos.writeBytes("exit\n");
            dos.flush();
            String line = null;
            while ((line = dis.readLine()) != null) {
                result += line;
            }
            p.waitFor();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (dos != null) {
                try {
                    dos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (dis != null) {
                try {
                    dis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return result;
    }

    /**
     * 唤醒屏幕
     *
     * @param context
     */
    public static void wakeUp(Context context) {
        PowerManager powerManager = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        try {
            powerManager.getClass().getMethod("wakeUp", new Class[]{long.class}).invoke(powerManager, SystemClock.uptimeMillis());
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
    }


    public static int shutdown() {
        int r = 0;
        try {
//             Process process = Runtime.getRuntime().exec(new String[]{"su", "-c", "reboot -p"});
            //Process process = Runtime.getRuntime().exec("su -c \"/system/bin/shutdown\"");
            Process process = Runtime.getRuntime().exec("su -c reboot");
            r = process.waitFor();
            System.out.println("r:" + r);
        } catch (IOException e) {
            e.printStackTrace();
            r = -1;
        } catch (InterruptedException e) {
            e.printStackTrace();
            r = -1;
        }
        return r;
    }

    public static void doSU() {
        try {
            Process process = Runtime.getRuntime().exec("su");// (这里执行是系统已经开放了root权限，而不是说通过执行这句来获得root权限)
            DataOutputStream os = new DataOutputStream(process.getOutputStream());
            os.writeBytes("exit\n");
            os.flush();
            //如果已经root，但是用户选择拒绝授权,e.getMessage() = write failed: EPIPE (Broken pipe)
            //如果没有root，,e.getMessage()= Error running exec(). Command: [su] Working Directory: null Environment: null
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
