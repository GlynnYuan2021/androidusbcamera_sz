package com.cnsj.cuiniaopad.application;

import android.app.Application;

import com.cnsj.cuiniaopad.utils.CrashHandler;
import com.cnsj.cuiniaopad.utils.CrashHandler1;

/**application class
 *
 * Created by jianddongguo on 2017/7/20.
 */

public class MyApplication extends Application {
    private CrashHandler mCrashHandler;
    // File Directory in sd card
    public static final String DIRECTORY_NAME = "USBCamera";

    @Override
    public void onCreate() {
        super.onCreate();
//        mCrashHandler = CrashHandler.getInstance();
//        mCrashHandler.init(getApplicationContext(), getClass());

        CrashHandler1 crashHandler= CrashHandler1.getInstance();
        crashHandler.init(this);
    }
}
