package com.cnsj.cuiniaopad.network;



import com.cnsj.cuiniaopad.network.apiService.APIFunction;
import com.cnsj.cuiniaopad.network.config.NetConfig;
import com.cnsj.cuiniaopad.network.config.UrlConfig;
import com.cnsj.cuiniaopad.network.download.JsDownloadInterceptor;
import com.cnsj.cuiniaopad.network.download.JsDownloadListener;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.TimeUnit;


import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

/**
 * create time :2019/4/13
 * author :steadyxia
 * description:
 */
public class DownloadFactory {

    private static DownloadFactory mRetrofitFactory;
    private APIFunction mApi;
    private Retrofit retrofitBuild;
    private JsDownloadListener listener;

    public static DownloadFactory getInstance(JsDownloadListener listener) {
        if (mRetrofitFactory == null) {
            synchronized (RetrofitFactory.class) {
                mRetrofitFactory = new DownloadFactory(listener);
            }
        }
        return mRetrofitFactory;
    }

    private DownloadFactory(JsDownloadListener listener) {
        this.listener = listener;
        initNet(listener);
    }

    private void initNet(JsDownloadListener listener) {

        OkHttpClient okHttpBuild = new OkHttpClient.Builder()
                .connectTimeout(NetConfig.HTTP_CONNECT_TIMEOUT, TimeUnit.SECONDS)//设置连接超时时间
                .readTimeout(NetConfig.HTTP_READ_TIMEOUT, TimeUnit.SECONDS)//设置读取超时时间
                .writeTimeout(NetConfig.HTTP_WRITE_TIMEOUT, TimeUnit.SECONDS)//设置写入超时时间
                .addInterceptor(new JsDownloadInterceptor(listener))
                .build();

        //add 支持RxJava2
        retrofitBuild = new Retrofit.Builder()
                .baseUrl(UrlConfig.BASE_URL)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())//add 支持RxJava2
                .client(okHttpBuild)
                .build();
    }


    public void download(String url, final File file, Observer<InputStream> observable) {
        retrofitBuild.create(APIFunction.class).downLoadFile(url)
                .subscribeOn(Schedulers.io())
                .unsubscribeOn(Schedulers.io())
                .map(new Function<ResponseBody, InputStream>() {
                    @Override
                    public InputStream apply(ResponseBody responseBody) throws Exception {
                        return responseBody.byteStream();
                    }
                })
                .observeOn(Schedulers.computation()) // 用于计算任务
                .doOnNext(new Consumer<InputStream>() {
                    @Override
                    public void accept(InputStream inputStream) throws Exception {
                        writeFile(inputStream, file);
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(observable);


    }


    private void writeFile(InputStream inputString, File file) {
        if (file.exists()) {
            file.delete();
        }

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(file);

            byte[] b = new byte[1024 * 128];

            int len;
            while ((len = inputString.read(b)) != -1) {
                fos.write(b, 0, len);
            }
            inputString.close();
            fos.close();

        } catch (FileNotFoundException e) {
            listener.onFail("FileNotFoundException");
        } catch (IOException e) {
            listener.onFail("IOException");
        }
    }


}
