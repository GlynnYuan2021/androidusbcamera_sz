package com.cnsj.cuiniaopad.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.SurfaceTexture;
import android.util.AttributeSet;
import android.view.Surface;

import com.serenegiant.usb.encoder.IVideoEncoder;
import com.serenegiant.usb.widget.AspectRatioTextureView;
import com.serenegiant.usb.widget.CameraViewInterface;

public class UVCTextureView extends AspectRatioTextureView implements CameraViewInterface {


    public UVCTextureView(Context context) {
         this(context, null, 0);
    }

    public UVCTextureView(Context context, AttributeSet attrs) {
       this(context, attrs, 0);
    }

    public UVCTextureView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setSurfaceTextureListener(textureListenerListener);
    }

    /**
     * ==CameraViewInterface==========================================================================================
     */

    @Override
    public void onPause() {

    }

    @Override
    public void onResume() {

    }

    @Override
    public void setCallback(Callback callback) {

    }

    @Override
    public SurfaceTexture getSurfaceTexture() {
        return null;
    }

    @Override
    public Surface getSurface() {
        return null;
    }

    @Override
    public boolean hasSurface() {
        return false;
    }

    @Override
    public void setVideoEncoder(IVideoEncoder encoder) {

    }

    @Override
    public Bitmap captureStillImage(int width, int height) {
        return null;
    }

    @Override
    public void setAspectRatio(double v) {

    }

    @Override
    public void setAspectRatio(int i, int i1) {

    }

    @Override
    public double getAspectRatio() {
        return 0;
    }



    /**
     * ==TextureView.SurfaceTextureListener==========================================================================================
     */

    SurfaceTextureListener textureListenerListener = new SurfaceTextureListener() {
        @Override
        public void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int i, int i1) {//当TextureView的SurfaceTexture准备好使用时调用。

        }

        @Override
        public void onSurfaceTextureSizeChanged(SurfaceTexture surfaceTexture, int i, int i1) {//当SurfaceTexture的缓冲区大小更改时调用。

        }

        @Override
        public boolean onSurfaceTextureDestroyed(SurfaceTexture surfaceTexture) {//当指定的SurfaceTexture将要销毁时调用。
            return false;
        }

        @Override
        public void onSurfaceTextureUpdated(SurfaceTexture surfaceTexture) {//通过UpdateTexImage（）更新 指定的SurfaceTexture时调用。

        }
    };
}
