package com.cnsj.cuiniaopad.network.config;

/**
 * Create time : 2019-1-11 0011
 * Author : steadyxia
 * Description : app 访问接口类
 */
public class UrlConfig {

    // public static final String BASE_URL = "https://lcloud.lzz56.com/api/";

    //public static final String BASE_URL = "http://127.0.0.1:80/";

    public static final String BASE_URL = "http://121.89.223.217:10086/";

    //版本更新
    public static final String upDateVersion = "updatequery";

    //  test  http://a.xzfile.com//anzhuo/huaweiyingyongshichangv11.0.1.301_downcc.com.apk
    public static final String downloadUrl = "http://121.89.218.101:8080/download/usbcamera_2.apk";


    //public static final String updataVersion = "common/latest/version";


}
