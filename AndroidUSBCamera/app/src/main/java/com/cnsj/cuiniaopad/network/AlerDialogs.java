package com.cnsj.cuiniaopad.network;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.cube.AllDevicePolicyManager;
import android.os.Environment;
import android.text.TextUtils;
import android.util.Log;


import com.blankj.utilcode.util.AppUtils;
import com.blankj.utilcode.util.FileUtils;
import com.cnsj.cuiniaopad.network.download.JsDownloadListener;
import com.cnsj.cuiniaopad.utils.SystemUtils;

import java.io.File;
import java.io.InputStream;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

/**
 * create time :2019/4/12
 * author :steadyxia
 * description:
 */
public class AlerDialogs {

    public static AlertDialog.Builder tipsUpData(final Context context, String msg, final String url, int isForcedUpdata) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("有新版本，是否更新？");
        builder.setMessage(msg);
        //点击对话框以外的区域是否让对话框消失
        builder.setCancelable(false);

        if (isForcedUpdata == 1) {//强制更新，不可取消
            builder.setPositiveButton("确认", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    //进行下载操作
                    progressDailog(context, url, msg);

                }
            });

        } else if (isForcedUpdata == 0) {//非强制更新，可取消

            builder.setPositiveButton("确认", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    //进行下载操作
                    progressDailog(context, url, msg);

                }
            });
            builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
        }
        return builder;
    }

    public static ProgressDialog progressDailog(Context context, String url, String msg) {
        final ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progressDialog.setCancelable(false);// 设置是否可以通过点击Back键取消
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setTitle("正在下载");
        progressDialog.setMessage(msg);
        progressDialog.setProgressNumberFormat("");
        progressDialog.show();


        DownloadFactory instance = DownloadFactory.getInstance(new JsDownloadListener() {
            @Override
            public void onStartDownload(long length) {
            }

            @Override
            public void onProgress(int progress) {
                progressDialog.setProgress(progress);
            }

            @Override
            public void onFail(String errorInfo) {

            }
        });

        String PATH_CHALLENGE_APK = Environment.getExternalStorageDirectory() + "/DownloadFile";
        String name = url;
        String mApkPath = null;
        if (FileUtils.createOrExistsDir(PATH_CHALLENGE_APK)) {
            int i = name.lastIndexOf('/');//一定是找最后一个'/'出现的位置
            if (i != -1) {
                name = name.substring(i);
                mApkPath = PATH_CHALLENGE_APK + name;
            }
        } else {
            Log.e("TAG", "downloadApk: 创建失败");
        }
        if (TextUtils.isEmpty(mApkPath)) {
            Log.e("TAG", "downloadApk: 存储路径为空了");
        }
        //建立一个文件
        final File mFile = new File(mApkPath);

        if (FileUtils.isFileExists(mFile)) {
            FileUtils.delete(mFile);
        }

        instance.download(url, mFile, new Observer<InputStream>() {
            @Override
            public void onSubscribe(Disposable d) {
            }

            @Override
            public void onNext(InputStream inputStream) {
            }

            @Override
            public void onError(Throwable e) {
                Log.e("TAG", e.toString());

            }

            @Override
            public void onComplete() {
                //安装apk
                //AppUtils.installApp(mFile);

                //静默安装
                AllDevicePolicyManager mAllDevicePolicyManager = (AllDevicePolicyManager) context.getSystemService("cube");
                mAllDevicePolicyManager.silentInstall(mFile.getPath());


                progressDialog.dismiss();

            }
        });


        return progressDialog;
    }


}
