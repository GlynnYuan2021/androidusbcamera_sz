package com.cnsj.cuiniaopad.network;



import com.cnsj.cuiniaopad.network.apiService.APIFunction;
import com.cnsj.cuiniaopad.network.config.NetConfig;
import com.cnsj.cuiniaopad.network.config.UrlConfig;
import com.cnsj.cuiniaopad.network.interceptor.InterceptorLogging;

import java.util.concurrent.TimeUnit;


import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Create time : 2019-1-11 0011
 * Author : steadyxia
 * Description : Retrofit 管理工厂，网络入口、单利
 */
public class RetrofitFactory {

    private static RetrofitFactory mRetrofitFactory;
    private APIFunction mApi;

    public static RetrofitFactory getInstance() {
        if (mRetrofitFactory == null) {
            synchronized (RetrofitFactory.class) {
                mRetrofitFactory = new RetrofitFactory();
            }
        }
        return mRetrofitFactory;
    }

    private RetrofitFactory() {
        initNet();
    }

    private void initNet() {

        OkHttpClient okHttpBuild = new OkHttpClient.Builder()
                .connectTimeout(NetConfig.HTTP_CONNECT_TIMEOUT, TimeUnit.SECONDS)//设置连接超时时间
                .readTimeout(NetConfig.HTTP_READ_TIMEOUT, TimeUnit.SECONDS)//设置读取超时时间
                .writeTimeout(NetConfig.HTTP_WRITE_TIMEOUT, TimeUnit.SECONDS)//设置写入超时时间
                .addNetworkInterceptor(new HttpLoggingInterceptor(new InterceptorLogging()).setLevel(HttpLoggingInterceptor.Level.BODY))//网络日志拦截器
                .build();

        Retrofit retrofitBuild = new Retrofit.Builder()
                .baseUrl(UrlConfig.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())//add Gson转换器
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())//add 支持RxJava2
                .client(okHttpBuild)
                .build();
        mApi = retrofitBuild.create(APIFunction.class);
    }

    public APIFunction getRetrofitAPI() {
        return mApi;
    }
}
