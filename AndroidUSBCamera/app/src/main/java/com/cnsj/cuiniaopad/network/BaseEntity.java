package com.cnsj.cuiniaopad.network;

/**
 * Create time : 2019-1-16 0016
 * Author : steadyxia
 * Description :
 */
public class BaseEntity<T> {


    private String code;
    private String api;
    private T message;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getApi() {
        return api;
    }

    public void setApi(String api) {
        this.api = api;
    }

    public T getMessage() {
        return message;
    }

    public void setMessage(T message) {
        this.message = message;
    }
}
