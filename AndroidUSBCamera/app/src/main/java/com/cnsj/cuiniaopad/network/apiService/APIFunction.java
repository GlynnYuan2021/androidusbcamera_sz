package com.cnsj.cuiniaopad.network.apiService;


import io.reactivex.Observable;
import okhttp3.ResponseBody;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;
import retrofit2.http.Streaming;
import retrofit2.http.Url;

/**
 * Create time : 2019-1-11 0011
 * Author : steadyxia
 * Description :  API 接口类
 */
public interface APIFunction {

//    /**
//     * 提示更新
//     * @param pid
//     * @param mobile
//     * @param mac
//     * @param version
//     * @return
//     */
//    @FormUrlEncoded
//    @POST(UrlConfig.upDateVersion)
//    Observable<UpDateQueryResult> updateVersion(@Field("pid") String pid,
//                                                @Field("mobile") String mobile,
//                                                @Field("mac") String mac,
//                                                @Field("version") String version);
//
//    @POST(UrlConfig.upDateVersion)
//    Observable<UpDateQueryResult> updateVersion1(@Query("pid") String pid,
//                                                 @Query("mobile") String mobile,
//                                                 @Query("mac") String mac,
//                                                 @Query("version") String version);


    /**
     * 下载文件
     *
     * @param url
     * @return
     */
    @Streaming
    @GET
    Observable<ResponseBody> downLoadFile(@Url String url);
}
