package com.cnsj.cuiniaopad.view;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;

import com.cnsj.cuiniaopad.R;
import com.cnsj.cuiniaopad.utils.SystemUtils;

public class SuTestActivty extends Activity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_test_su);

        findViewById(R.id.btnFinish).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                 SystemUtils.doSU();
            }
        });

        findViewById(R.id.btn3).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {//息屏
                SystemUtils.power();
            }
        });
        findViewById(R.id.btn4).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {//关机
                SystemUtils.shutdown();
            }
        });

        findViewById(R.id.btn5).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {//关机
                SystemUtils.execRootCmd("pm install /storage/emulated/0/com.huawei.appmarket.2010231801.apk");
            }
        });
        findViewById(R.id.btn6).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {//关机
                SystemUtils.execRootCmd("input keyevent 3");
            }
        });
        findViewById(R.id.btn7).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {//关机
                SystemUtils.execRootCmd("input keyevent 4");
            }
        });

    }
}
